dt="$(date --iso-8601=seconds)"
curl -X POST -H "Accept: application/vnd.github.v3+json" -H "Authorization: token $GITHUB_AUTH" $API_REPO_URL -d '{"body":"$dt"}'
